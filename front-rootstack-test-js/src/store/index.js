import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router/index'

const axios = require('axios').default;
axios.defaults.baseURL = 'https://coding-test.rootstack.net/api/';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    jobs: {},
    auth: '',
    me: []
  },
  mutations: {
    setToken(state, payload){
      state.token = payload
    },
    setAuth(state, payload){
      state.auth = payload
    },
    setJobs(state, payload){
      state.jobs = payload
    },
    setMe(state, payload){
      state.me = payload
    },
  },
  actions: {
    /* login action function ***********************************************/
    login({commit,state}, user){
      axios({
        method: 'post',
        url: 'auth/login',
        data: user
      }).then(function (response) {
        commit('setToken', response.data.access_token)
        commit('setAuth', 'true')
        localStorage.setItem('token', response.data.access_token)
        router.replace('/about').catch(err => {})
      })
      .catch(function (error) {
        console.log("login", error);
      });
    },

    /* get jobs data *******************************************************/
    jobsAction({commit}){
      axios({
        method: 'get',
        url: 'jobs',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+localStorage.getItem('token')
        }
      }).then(function (response) {
        //console.log('get jobs',response.data.data)
        commit('setJobs', response.data.data)
      })
      .catch(function (error) {
        console.log("get jobs", error);
      });
    },
    /* get me data *******************************************************/
    meAction({commit}){
      axios({
        method: 'get',
        url: 'auth/me',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+localStorage.getItem('token')
        }
      }).then(function (response) {
        //console.log('get me',response.data)
        commit('setMe', response.data)
      })
      .catch(function (error) {
        console.log("get me", error);
      });
    },
    /* token check *********************************************************/
    tokencheck({commit}){
      if(localStorage.getItem('token')!=''){
        commit('setAuth', 'true')
        router.push('/about').catch(err => {})
      }
    },
    /* close session ***************************************************** */
    logout({commit}){
      localStorage.removeItem('token')
      commit('setToken', null)
      router.push('/').catch(err => {})
    }
  },
  getters: {
    gettJobs: state =>{
        return state.jobs
    },
    gettAuth: state =>{
      return state.auth
    }
  },  
  modules: {
  }
})

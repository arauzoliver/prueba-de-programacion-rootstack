import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import * as VueGoogleMaps from "vue2-google-maps"

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAZVxJ2pWVz5zbYAoMAzn9AD6BsIOX3bqs",
    libraries: "places" // necessary for places input
  },
  installComponents: true
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

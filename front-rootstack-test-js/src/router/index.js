import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    meta: {protectedRoute: true}
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

/* router.beforeEach((to, from, next)=>{
const protectedRoute = to.matched.some( item=> item.meta.protectedRoute)
if(protectedRoute && localStorage.getItem('token')==''){
  next({
    path: '/',
    query: { redirect: to.fullPath }
  })
}else{
  next()
}
})
 */
router.beforeEach((to, from, next) => {
  if (to.matched.some( item=> item.meta.protectedRoute)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (localStorage.getItem('token')==null) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
